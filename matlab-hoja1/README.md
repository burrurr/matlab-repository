# Las siguientes reglas son obligatorias

1.  El alumno encuentra  para cada práctica un documento en formato **mlx**

2.  Tiene que bajar dicho fichero a su ordenador y ponerlo en una
    carpeta: cada práctica en una carpeta.

3.  Las carpetas y los ficheros no deben tener **espacio** ni
    **acentos** ni **-**, (**\_** están permitidos).

4.  Tiene que poner Nombre, Apellido y DNI en los campos correspondientes.

5.  El alumno tiene que responder a los ejercicios poniendo los
    comandos de matlab relevantes, en un campo, usando el comando
    *insert code* (control+e). Siempre al principio del campo tiene que poner
    
    `disp('Hoja X: Ej Y Nombre y apellido')`

6.  Cada campo tiene que terminar un un **section break** (Control+Alt+Enter).

7.  El alumno tiene que implementar ciertos algoritmos, cuyo
    pseudocódigo se encuentra en el CV. Dicha implementación debe
    ser una **function** que tiene que **siempre** estar al final del
    fichero mlx, en el apéndice. La sintaxis es algo así:
    
    `[t,y]=mieuler(f,intv,y0,N)`
    
    ènd`
    
    El end al final es muy importante. También dicha función debe
    tener
    `disp('Hoja X: func: Euler Nombre y Apellido')`

8.  El alumno tiene que entregar el fichero **mlx** pero también
    salvar el fichero con la extensión **m** pero con el sufijo text,
    por ejemplo: hoja7.mlx y hoja7<sub>text.m</sub>.

9.  Las entregas de prácticas no cuentan para la nota pero, si el
    alumno ha entregado prácticas a tiempo, las puede usar en el
    examen. Si no entrega prácticas no puede llevar nada al examen.


# Prácticas de Matlab (Distribución temporal de las hojas de Matlab)

<table border="2"cellspacing="0"cellpadding="6"rules="groups"frame=äll">


<colgroup>
<col  class=örg-right"/>
</colgroup>

<colgroup>
<col  class=örg-left"/>
</colgroup>

<colgroup>
<col  class=örg-left"/>
</colgroup>

<colgroup>
<col  class=örg-left"/>
</colgroup>

<colgroup>
<col  class=örg-left"/>
</colgroup>
<thead>
<tr>
<th scope="col"class=örg-right">Hoja</th>
<th scope="col"class=örg-left">Contenido</th>
<th scope="col"class=örg-left">Publicación</th>
<th scope="col"class=örg-left">Objetivo</th>
<th scope="col"class=örg-left">Entrega</th>
</tr>


<tr>
<th scope="col"class=örg-right">&#xa0;</th>
<th scope="col"class=örg-left">&#xa0;</th>
<th scope="col"class=örg-left">aproximadamente</th>
<th scope="col"class=örg-left">&#xa0;</th>
<th scope="col"class=örg-left">&#xa0;</th>
</tr>
</thead>

<tbody>
<tr>
<td class=örg-right">1</td>
<td class=örg-left">Ode45</td>
<td class=örg-left"><span class="timestamp-wrapper"><span class="timestamp"> 24.01.2023 </span></span></td>
<td class=örg-left">aprender ode45, sprintf, title</td>
<td class=örg-left">opcional</td>
</tr>
</tbody>

<tbody>
<tr>
<td class=örg-right">2</td>
<td class=örg-left">Bucles simples</td>
<td class=örg-left"><span class="timestamp-wrapper"><span class="timestamp"> 24.01.2023 </span></span></td>
<td class=örg-left">aprender a usar bucles sin índices</td>
<td class=örg-left">opcional</td>
</tr>
</tbody>

<tbody>
<tr>
<td class=örg-right">3</td>
<td class=örg-left">Euler/RK</td>
<td class=örg-left"><span class="timestamp-wrapper"><span class="timestamp"> 30.01.2023 </span></span></td>
<td class=örg-left">Implementar métodos monopaso</td>
<td class=örg-left"><span class="timestamp-wrapper"><span class="timestamp"> 13.02.2023 </span></span></td>
</tr>


<tr>
<td class=örg-right">&#xa0;</td>
<td class=örg-left">&#xa0;</td>
<td class=örg-left">&#xa0;</td>
<td class=örg-left">explícitos</td>
<td class=örg-left">&#xa0;</td>
</tr>
</tbody>

<tbody>
<tr>
<td class=örg-right">4</td>
<td class=örg-left">Diagrama de eficiencia</td>
<td class=örg-left"><span class="timestamp-wrapper"><span class="timestamp"> 06.02.2023 </span></span></td>
<td class=örg-left">Construir diagramas para calcular</td>
<td class=örg-left"><span class="timestamp-wrapper"><span class="timestamp"> 20.02.2023 </span></span></td>
</tr>


<tr>
<td class=örg-right">&#xa0;</td>
<td class=örg-left">&#xa0;</td>
<td class=örg-left">&#xa0;</td>
<td class=örg-left">el orden de un método</td>
<td class=örg-left">&#xa0;</td>
</tr>
</tbody>

<tbody>
<tr>
<td class=örg-right">5</td>
<td class=örg-left">Métodos implícitos</td>
<td class=örg-left"><span class="timestamp-wrapper"><span class="timestamp"> 20.02.2023 </span></span></td>
<td class=örg-left">Implementar métodos monopaso</td>
<td class=örg-left"><span class="timestamp-wrapper"><span class="timestamp"> 20.03.2023 </span></span></td>
</tr>


<tr>
<td class=örg-right">&#xa0;</td>
<td class=örg-left">por Newton y punto fijo</td>
<td class=örg-left">&#xa0;</td>
<td class=örg-left">implícitos mediante</td>
<td class=örg-left">&#xa0;</td>
</tr>


<tr>
<td class=örg-right">&#xa0;</td>
<td class=örg-left">&#xa0;</td>
<td class=örg-left">&#xa0;</td>
<td class=örg-left">dos esquemas: punto fijo y Newton;</td>
<td class=örg-left">&#xa0;</td>
</tr>


<tr>
<td class=örg-right">&#xa0;</td>
<td class=örg-left">&#xa0;</td>
<td class=örg-left">&#xa0;</td>
<td class=örg-left">diagramas de</td>
<td class=örg-left">&#xa0;</td>
</tr>


<tr>
<td class=örg-right">&#xa0;</td>
<td class=örg-left">&#xa0;</td>
<td class=örg-left">&#xa0;</td>
<td class=örg-left">eficiencia</td>
<td class=örg-left">&#xa0;</td>
</tr>


<tr>
<td class=örg-right">&#xa0;</td>
<td class=örg-left">&#xa0;</td>
<td class=örg-left">&#xa0;</td>
<td class=örg-left">&#xa0;</td>
<td class=örg-left">&#xa0;</td>
</tr>
</tbody>

<tbody>
<tr>
<td class=örg-right">6</td>
<td class=örg-left">Métodos Multipaso</td>
<td class=örg-left"><span class="timestamp-wrapper"><span class="timestamp"> 20.03.2023 </span></span></td>
<td class=örg-left">Implementar un método multipaso</td>
<td class=örg-left"><span class="timestamp-wrapper"><span class="timestamp"> 27.03.2023 </span></span></td>
</tr>


<tr>
<td class=örg-right">&#xa0;</td>
<td class=örg-left">&#xa0;</td>
<td class=örg-left">&#xa0;</td>
<td class=örg-left">implícito mediante</td>
<td class=örg-left">&#xa0;</td>
</tr>


<tr>
<td class=örg-right">&#xa0;</td>
<td class=örg-left">&#xa0;</td>
<td class=örg-left">&#xa0;</td>
<td class=örg-left">Newton</td>
<td class=örg-left">&#xa0;</td>
</tr>
</tbody>

<tbody>
<tr>
<td class=örg-right">7</td>
<td class=örg-left">Adaptativos</td>
<td class=örg-left"><span class="timestamp-wrapper"><span class="timestamp"> 27.03.2023 </span></span></td>
<td class=örg-left">Implementar métodos monopaso con</td>
<td class=örg-left"><span class="timestamp-wrapper"><span class="timestamp"> 17.04.2023 </span></span></td>
</tr>


<tr>
<td class=örg-right">&#xa0;</td>
<td class=örg-left">&#xa0;</td>
<td class=örg-left">&#xa0;</td>
<td class=örg-left">paso adaptativo</td>
<td class=örg-left">&#xa0;</td>
</tr>


<tr>
<td class=örg-right">&#xa0;</td>
<td class=örg-left">&#xa0;</td>
<td class=örg-left">&#xa0;</td>
<td class=örg-left">usando h<sub>opt</sub> y pares encajados</td>
<td class=örg-left">&#xa0;</td>
</tr>
</tbody>

<tbody>
<tr>
<td class=örg-right">8</td>
<td class=örg-left">Locus</td>
<td class=örg-left"><span class="timestamp-wrapper"><span class="timestamp"> 10.04.2023 </span></span></td>
<td class=örg-left">usando cálculo simbólico, pintar</td>
<td class=örg-left">opcional</td>
</tr>


<tr>
<td class=örg-right">&#xa0;</td>
<td class=örg-left">&#xa0;</td>
<td class=örg-left">&#xa0;</td>
<td class=örg-left">regiones de</td>
<td class=örg-left">&#xa0;</td>
</tr>


<tr>
<td class=örg-right">&#xa0;</td>
<td class=örg-left">&#xa0;</td>
<td class=örg-left">&#xa0;</td>
<td class=örg-left">estabilidad</td>
<td class=örg-left">&#xa0;</td>
</tr>
</tbody>

<tbody>
<tr>
<td class=örg-right">9</td>
<td class=örg-left">Disparo</td>
<td class=örg-left"><span class="timestamp-wrapper"><span class="timestamp"> 17.04.2023 </span></span></td>
<td class=örg-left">usando RK, implementar el disparo</td>
<td class=örg-left"><span class="timestamp-wrapper"><span class="timestamp"> 28.04.2023 </span></span></td>
</tr>


<tr>
<td class=örg-right">&#xa0;</td>
<td class=örg-left">&#xa0;</td>
<td class=örg-left">&#xa0;</td>
<td class=örg-left">lineal para</td>
<td class=örg-left">&#xa0;</td>
</tr>


<tr>
<td class=örg-right">&#xa0;</td>
<td class=örg-left">&#xa0;</td>
<td class=örg-left">&#xa0;</td>
<td class=örg-left">condiciones de Dirichlet y Neuman</td>
<td class=örg-left">&#xa0;</td>
</tr>
</tbody>
</table>

